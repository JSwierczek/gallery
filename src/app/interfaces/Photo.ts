import { PhotoLinks } from "./PhotoLinks"
import { Sponsorship } from "./SponsorShip"
import { TopicSubmissions } from "./TopicSubmissions"
import { Urls } from "./Urls"
import { User } from "./User"
import { Exif } from "./Exif"
import { Meta } from "./Meta"
import { Tag } from "./Tag"

export interface Photo {
    id: string
    created_at: string
    updated_at: string
    promoted_at: any
    width: number
    height: number
    color: string
    blur_hash: string
    description: any
    alt_description: any
    urls: Urls
    links: PhotoLinks
    likes: number
    liked_by_user: boolean
    current_user_collections: any[]
    sponsorship: Sponsorship
    topic_submissions: TopicSubmissions
    user: User
    exif: Exif
    location: Location
    meta: Meta
    public_domain: boolean
    tags: Tag[]
    tags_preview: any[]
    related_collections: any
    views: number
    downloads: number
    topics: any[]
}