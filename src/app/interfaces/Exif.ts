export interface Exif {
    make: any
    model: any
    name: any
    exposure_time: any
    aperture: any
    focal_length: any
    iso: any
}