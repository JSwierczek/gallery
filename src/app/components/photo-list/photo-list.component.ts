import { Component, Input } from '@angular/core';
import { Photo } from 'src/app/interfaces/Photo';

@Component({
  selector: 'app-photo-list',
  templateUrl: './photo-list.component.html',
  styleUrls: ['./photo-list.component.scss']
})
export class PhotoListComponent {

  constructor() { }

  @Input() heading: string;
  @Input() url: string;
  @Input() photoList: Photo[] = [];
  @Input() displaySpinner: boolean = false;
  @Input() displayButton: boolean = false;
  @Input() enableLoading: boolean = false;

}
