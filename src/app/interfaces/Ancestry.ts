export interface Ancestry {
    type: Type
    category: Category
    subcategory: Subcategory
}

interface Type {
    slug: string
    pretty_slug: string
}

interface Category {
    slug: string
    pretty_slug: string
}

interface Subcategory {
    slug: string
    pretty_slug: string
}