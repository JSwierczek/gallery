## Uruchomienie aplikacji lokalnie

Po wpisaniu `npm run start`, aplikacja uruchomi się lokalnie na porcie 4200.

## Uruchomienie aplikacji online

Projekt Gallery hostowany jest na github pages: [LINK](https://jswierczek213.github.io/gallery/home)

## Dodatkowa informacja

Zewnętrzne API Unsplash pozwala na max 50 requestów na godzinę. 
Następnie komunikacja się blokuje.