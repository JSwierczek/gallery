import { ProfileImage } from "./ProfileImage"
import { Social } from "./Social"
import { UserLinks } from "./UserLinks"

export interface User {
    id: string
    updated_at: string
    username: string
    name: string
    first_name: string
    last_name: any
    twitter_username: any
    portfolio_url: string
    bio: string
    location: any
    links: UserLinks
    profile_image: ProfileImage
    instagram_username: string
    total_collections: number
    total_likes: number
    total_photos: number
    accepted_tos: boolean
    for_hire: boolean
    social: Social
}