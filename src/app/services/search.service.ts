import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { environment } from '../../environments/environment';

@Injectable({
  providedIn: 'root'
})
export class SearchService {

  constructor(private http: HttpClient) { }

  // REST API keys
  accessKey: string = environment.accessKey;
  secretKey: string = environment.secretKey;

  // REST API
  api: string = 'https://api.unsplash.com';

  searchPhotos(
    query: string,
    page = 1,
    photosCount = 10,
    orderBy: 'latest' | 'relevant' = 'relevant',
    orientation: 'landscape' | 'portrait' | 'squerish' = 'landscape'
    ) {
    const headers: HttpHeaders = new HttpHeaders({ Authorization: 'Client-ID ' + this.accessKey });
    const url: string = `${this.api}/search/photos?query=${query}&page=${page}&per_page=${photosCount}&order_by=${orderBy}&orientation=${orientation}`;
    return this.http.get(url, { headers });
  }
}
