import { Sponsor } from "./Sponsor"

export interface Sponsorship {
    impression_urls: string[]
    tagline: string
    tagline_url: string
    sponsor: Sponsor
}