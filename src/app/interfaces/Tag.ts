import { Source } from "./Source"

export interface Tag {
    type: string
    title: string
    source?: Source
}