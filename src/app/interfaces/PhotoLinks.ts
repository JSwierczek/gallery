export interface PhotoLinks {
    self: string
    html: string
    download: string
    download_location: string
}