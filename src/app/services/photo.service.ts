import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { environment } from '../../environments/environment';
import { Observable } from 'rxjs';
import { Photo } from '../interfaces/Photo';
import { UnsplashStatistics } from '../interfaces/UnsplashStatistics';

@Injectable({
  providedIn: 'root'
})
export class PhotoService {

  constructor(private http: HttpClient) { }

  // REST API keys
  accessKey: string = environment.accessKey;
  secretKey: string = environment.secretKey;

  // REST API
  api: string = 'https://api.unsplash.com';

  getListFromAllPhotos(page = 1, photosCount = 10, orderBy: 'latest' | 'oldest' | 'popular' = 'latest'): Observable<Photo[]> {
    const headers: HttpHeaders = new HttpHeaders({ Authorization: 'Client-ID ' + this.accessKey });
    return this.http.get(`${this.api}/photos?page=${page}&per_page=${photosCount}&order_by=${orderBy}`, { headers }) as Observable<Photo[]>;
  }

  getSinglePhoto(photoId: string): Observable<Photo> {
    const headers = new HttpHeaders({ Authorization: 'Client-ID ' + this.accessKey });
    return this.http.get(`${this.api}/photos/${photoId}`, { headers }) as Observable<Photo>;
  }

  getRandomPhoto(orientation: 'portrait' | 'landscape' | 'squarish' = 'landscape'): Observable<Photo> {
    const headers = new HttpHeaders({ Authorization: 'Client-ID ' + this.accessKey });
    return this.http.get(`${this.api}/photos/random?orientation=${orientation}`, { headers }) as Observable<Photo>;
  }

  emitPhotoDownload(photoId: string): void {
    const headers = new HttpHeaders({ Authorization: 'Client-ID ' + this.accessKey });
    this.http.get(`${this.api}/photos/${photoId}/download`, { headers }).subscribe();
  }

  getUnsplashStatistics(type: 'total' | 'month'): Observable<UnsplashStatistics> {
    const headers = new HttpHeaders({ Authorization: 'Client-ID ' + this.accessKey });
    return this.http.get(`${this.api}/stats/${type}`, { headers }) as Observable<UnsplashStatistics>;
  }
}
